# Test Assessment - Issue Tracker API

## Intro

Mini Project ini bernama Issue Tracker API yang berfungsi sebagai service pencatatan bug dan issue yang ditemukan dalam sebuah proyek software.

Aplikasi ini dibangun menggunakan **ASP.NET CORE Web API** dengan menerapkan konsep **REST API** untuk backendnya.

## Arsitektur
Aplikasi ini menggunakan arsitektur **Microservices** di mana terdapat 3 buah services dengan masing-masing menggunakan RDBMS yang berbeda:
- Master Data Service - **MySQL**
- Issue Tracking Service - **PostgreSQL**
- Notification Service - **SQL Server**

Ketiga services tersebut disatukan dengan  **API Gateway** sehingga dapat diakses hanya dengan melalui single entry point.

## Tech
Berikut adalah detail dari tech atau tools yang digunakan dalam membuat aplikasi ini:

**Utama** : .NET 7 Web API, MySql, PostgreSQL, SQL Server

**Tools Pendukung**: EntityFramework, Automapper, Ocelot API Gateway, Swagger, Postman

## Struktur Folder
Terdapat 3 folder pada repository ini.

**Folder App** berisi source code dari aplikasi Issue Tracker API yang didalamnya terdapat 3 Project Service dan 1 Project API Gateway.

**Folder Queries** berisi Syntax SQL yang digunakan untuk membuat schema database dan juga kueri untuk melakukan inisialisasi data ke dalam database.

**Folder Postman Collection** berisi sample collection untuk melakukan simulasi REST API yang bisa dieksekusi menggunakan aplikasi Postman.