﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotificationService.Models.DTO;
using NotificationService.Models;

namespace NotificationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        public NotificationController(AppDbContext context, IMapper mapper)
        {
            _context=context;
            _mapper=mapper;
        }

        [HttpGet("GetLogs")]
        public ActionResult<IEnumerable<NotificationLog>> GetAll(int userId, int issueId)
        {
            if(userId == 0 && issueId == 0)
            {
                return _context.NotificationLogs;
            }
            return _context.NotificationLogs.Where(x=>x.IssueId == issueId || x.UserId == userId).ToList();
        }

        [HttpPost]
        public async Task<ActionResult> Create(NotifDTO dto)
        {
            var notif = _mapper.Map<NotificationLog>(dto);
            await _context.NotificationLogs.AddAsync(notif);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }

        [HttpPut("UpdateStatus/{notifId:int}")]
        public async Task<ActionResult> Update([FromBody] NotifStatusDTO dto, int notifId)
        {
            try
            {
                var notif = _context.NotificationLogs.Find(notifId);
                if (notif == null) return NotFound(new { status = 404, message = "Data Not Found" });

                notif.Status = dto.Status;
                _context.NotificationLogs.Update(notif);

                await _context.SaveChangesAsync();
                return Ok(new { status = 200, message = "Process success!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message.ToString() });
            }
        }
    }
}
