﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotificationService.Models;
using NotificationService.Models.DTO;

namespace NotificationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationTypeController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        public NotificationTypeController(AppDbContext context, IMapper mapper)
        {
            _context=context;
            _mapper=mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<NotificationType>> GetAll()
        {
            return _context.NotificationTypes;
        }

        [HttpPost]
        public async Task<ActionResult> Create(NotifTypeDTO dto)
        {
            var notifType = _mapper.Map<NotificationType>(dto);
            await _context.NotificationTypes.AddAsync(notifType);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }

        [HttpPut("{notifId:int}")]
        public async Task<ActionResult> Update([FromBody] NotifTypeDTO dto, int notifId)
        {
            try
            {
                var notifType = _mapper.Map<NotificationType>(dto);
                notifType.Id = notifId;
                _context.NotificationTypes.Update(notifType);

                if (await _context.NotificationTypes.FindAsync(notifId) == null) return NotFound(new { status = 404, message = "Data Not Found" });
                await _context.SaveChangesAsync();
                return Ok(new { status = 200, message = "Process success!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message.ToString() });
            }
        }

        [HttpDelete("{notifId:int}")]
        public async Task<ActionResult> Delete(int notifId)
        {
            var notifType = await _context.NotificationTypes.FindAsync(notifId);
            if (notifType == null) return NotFound(new { status = 404, message = "Data Not Found!" });

            _context.NotificationTypes.Remove(notifType);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }
    }
}
