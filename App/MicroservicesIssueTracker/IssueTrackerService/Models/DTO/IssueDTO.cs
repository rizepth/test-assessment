﻿using System.ComponentModel.DataAnnotations.Schema;

namespace IssueTrackerService.Models.DTO
{
    public class IssueDTO
    {
        public int ProjectId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }
        public int ReportedBy { get; set; }
    }
}
