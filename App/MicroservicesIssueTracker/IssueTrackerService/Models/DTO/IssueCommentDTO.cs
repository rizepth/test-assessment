﻿namespace IssueTrackerService.Models.DTO
{
    public class IssueCommentDTO
    {
        public int IssueId { get; set; }
        public string? Comment { get; set; }
        public int UserId { get; set; }
    }
}
