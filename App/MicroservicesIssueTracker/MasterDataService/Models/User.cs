﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasterDataService.Models
{
    [Table("user")]
    public class User : BaseEntity
    {
        [Key]
        [Column("user_id")]
        public int UserId { get; set; }
        [Column("user_name")]
        public string? UserName { get; set; }
        [Column("email")]
        public string? Email { get; set; }
        [Column("phone_number")]
        public string? PhoneNumber { get; set; }
    }
}
