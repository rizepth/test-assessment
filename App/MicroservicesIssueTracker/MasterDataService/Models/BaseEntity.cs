﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MasterDataService.Models
{
    public class BaseEntity
    {
        [Column("created_date")]
        public DateTime? CreatedDate { get; set; }
        [Column("created_by")]
        public string? CreatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
        [Column("updated_by")]
        public string? UpdatedBy { get; set; }
    }
}
