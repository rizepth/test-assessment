﻿namespace MasterDataService.Models.DTO
{
    public class RoleDTO
    {
        public string? RoleName { get; set; }
    }
}
