﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MasterDataService.Models
{
    [Table("role")]
    public class Role : BaseEntity
    {
        [Key]
        [Column("role_id")]
        public int RoleId { get; set; }
        [Column("role_name")]
        public string? RoleName { get; set; }
    }
}
