﻿using AutoMapper;
using MasterDataService.Models;
using MasterDataService.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        public RoleController(AppDbContext context, IMapper mapper)
        {
            _context=context;
            _mapper=mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Role>> GetAll()
        {
            return _context.Roles;
        }

        [HttpGet("{roleId:int}")]
        public async Task<ActionResult<Role>> GetById(int roleId)
        {
            var role = await _context.Roles.FindAsync(roleId);
            if (role == null) return NotFound(new { status = 404, message = "Data Not Found!" });
            return role;
        }

        [HttpPost]
        public async Task<ActionResult> Create(RoleDTO roleDto)
        {
            var role = _mapper.Map<Role>(roleDto);
            await _context.Roles.AddAsync(role);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }

        [HttpPut("{roleId:int}")]
        public async Task<ActionResult> Update([FromBody] RoleDTO roleDto, int roleId)
        {
            try
            {
                var role = _mapper.Map<Role>(roleDto);
                role.RoleId = roleId;
                _context.Roles.Update(role);

                if (await _context.Roles.FindAsync(roleId) == null) return NotFound(new { status = 404, message = "Data Not Found" });
                await _context.SaveChangesAsync();
                return Ok(new { status = 200, message = "Process success!" });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message.ToString() });
            }
        }

        [HttpDelete("{roleId:int}")]
        public async Task<ActionResult> Delete(int roleId)
        {
            var role = await _context.Roles.FindAsync(roleId);
            if (role == null) return NotFound(new { status = 404, message = "Data Not Found!" });

            _context.Roles.Remove(role);
            await _context.SaveChangesAsync();
            return Ok(new { status = 200, message = "Process success!" });
        }
    }
}
